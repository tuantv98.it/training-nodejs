const db = require('../../models/index.js');
const category = db.categories;
const product = db.products

category.hasMany(product, { foreignKey: "category_id" });

let getAll = async () => {
    return new Promise(async (resolve, reject) => {
        try {
            let data = await category.findAll();
            resolve(data);
        } catch (e) {
            reject(e)
        }
    })
}

let getDetail = async (id) => {
    return new Promise(async (resolve, reject) => {
        try {
            let data = await category.findOne({
                where: {
                    id: id,
                },
            });
            resolve(data);
        } catch (e) {
            reject(e)
        }
    })
}

let store = async (body) => {
    return new Promise(async (resolve, reject) => {
        try {
            let data = await category.create(body);
            resolve(data);
        } catch (e) {
            reject(e)
        }
    })
}

let update = async (body) => {
    return new Promise(async (resolve, reject) => {
        try {
            let detail = await category.findOne({
                where: {
                    id: body.id,
                },
            });

            if (detail) {
                const dataUpdate = {
                    product_name: body.product_name,
                }
                let update = await detail.update(dataUpdate)
                resolve(update);
            }
        } catch (e) {
            reject(e)
        }
    })
}

let destroy = async (id) => {
    return new Promise(async (resolve, reject) => {
        try {
            let destroy = await category.destroy({
                where: {
                    id: id,
                },
            });
            resolve(destroy);
        } catch (e) {
            reject(e)
        }
    })
}

module.exports = {
    getAll: getAll,
    store: store,
    getDetail: getDetail,
    update: update,
    destroy: destroy,
}