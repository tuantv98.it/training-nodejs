const db = require('../../models/index.js');
const product = db.products;

product.belongsTo(db.categories, { foreignKey: "category_id" , as:"cate",});

let getAll = async () => {
    return new Promise(async (resolve, reject) => {
        try {
            let data = await product.findAll(
                {
                    include : [{
                        model: db.categories,
                        as:'cate',
                    }],
                    attributes : {
                      exclude : ['createdAt','updatedAt']
                    },
                    raw:true,
                    nest:true
                }
            );
            resolve(data);
        } catch (e) {
            reject(e)
        }
    })
}

let getDetail = async (id) => {
    return new Promise(async (resolve, reject) => {
        try {
            let data = await product.findOne({
                where: {
                    id: id,
                },
            });
            resolve(data);
        } catch (e) {
            reject(e)
        }
    })
}

let store = async (body) => {
    return new Promise(async (resolve, reject) => {
        try {
            let data = await product.create(body);
            resolve(data);
        } catch (e) {
            reject(e)
        }
    })
}

let update = async (body) => {
    return new Promise(async (resolve, reject) => {
        try {
            let detail = await product.findOne({
                where: {
                    id: body.id,
                },
            });

            if (detail) {
                const dataUpdate = {
                    product_name: body.product_name,
                }
                let update = await detail.update(dataUpdate)
                resolve(update);
            }
        } catch (e) {
            reject(e)
        }
    })
}

let destroy = async (id) => {
    return new Promise(async (resolve, reject) => {
        try {
            let destroy = await product.destroy({
                where: {
                    id: id,
                },
            });
            resolve(destroy);
        } catch (e) {
            reject(e)
        }
    })
}

module.exports = {
    getAll: getAll,
    store: store,
    getDetail: getDetail,
    update: update,
    destroy: destroy,
}