const ResponseJson = require('../support/ResponseResource.js');
const CategoryService = require('../services/CategoryService');

const getAll = async (req, res) => {
    const data = await CategoryService.getAll();
    return ResponseJson.response(res, data)
}

const getDetail = async (req, res) => {
    const data = await CategoryService.getDetail(req.params.product_id);
    return ResponseJson.response(res, data)
}

const store = async (req, res) => {
    const data = await CategoryService.store(req.body);
    return ResponseJson.response(res, data)
}

const update = async (req, res) => {
    const data = await CategoryService.update(req.body);
    return ResponseJson.response(res, data)
}

const destroy = async (req, res) => {
    const data = await CategoryService.destroy(req.params.product_id);
    return ResponseJson.response(res, data)
}


module.exports = {
    getAll: getAll,
    getDetail: getDetail,
    store: store,
    update: update,
    destroy: destroy,
}