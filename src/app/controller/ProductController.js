const db = require('../../models/index.js');
const ResponseJson = require('../support/ResponseResource.js');
const ProductService = require('../services/ProductService');
const ProductValidate = require('../validation/ProductValidate');
const { body, validationResult } = require('express-validator');
const helper = require("../helper");
const getProduct = async (req, res) => {
    const data = await ProductService.getAll();
    return ResponseJson.response(res, data)
}

const getDetail = async (req, res) => {
    const data = await ProductService.getDetail(req.params.product_id);
    return ResponseJson.response(res, data)
}

const store = async (req, res) => {
    const data = await ProductService.store(req.body);
    return ResponseJson.response(res, data)
}

const update = async (req, res) => {
    const data = await ProductService.update(req.body);
    return ResponseJson.response(res, data)
}

const destroy = async (req, res) => {
    const data = await ProductService.destroy(req.params.product_id);
    if (data) {
        return ResponseJson.response(res, data)
    }
    return ResponseJson.error(res)
}


module.exports = {
    getProduct: getProduct,
    getDetail: getDetail,
    store: store,
    update: update,
    destroy: destroy,
}