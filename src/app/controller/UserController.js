const ResponseJson = require('../support/ResponseResource.js');
const UserService = require('../services/UserService');
const db = require('../../models/index.js');
require("dotenv").config();
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const login = async (req, res) => {
    try {
        const user_name = req.body.user_name;
        const password = req.body.password;
        const user = await db.users.findOne({
            where: {
                user_name: req.body.user_name
            }
        });
        if (!user) {
            return res.json({
                message: "Invalid email or password"
            })
        }

        const isValidPassword = await bcrypt.compare(password, user.password)
        console.log(isValidPassword)
        if (isValidPassword) {
            // save user token
            const token = jwt.sign({user_id: user.id, user_name}, process.env.SYSTEM_SECRET_KEY, {
                expiresIn: "2h",
            })

            const refreshToken = jwt.sign({
                user_id: user.id, user_name
            }, process.env.REFRESH_TOKEN_SECRET, {expiresIn: process.env.REFRESH_TOKEN_LIFE})
            const data = {
                user: user, token: token, refresh_token: refreshToken
            }
            res.status(200).json(data);
        } else {
            ResponseJson.unauthorized(res)
        }
    } catch (e) {
        console.log(e);
    }
}

module.exports = {
    login: login,
}