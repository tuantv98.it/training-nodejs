const Validator = require('validatorjs');

const validation = async (body, rules, customMessages, callback) => {
    const validation = new Validator(body, rules, customMessages);
    validation.passes(() => callback(null, true));
    validation.fails(() => callback(validation.errors, false));
};

/**
 * Get form data request
 * @param req
 *
 */
const getFormData = (req) => {
    try {
        let body = req.body;
        if (req.files){
            req.files.forEach(element => {
                body[element.fieldname] = element.filename;
            })
        }
        return body;
    } catch (err) {
        console.log(err);
    }
};

module.exports = {
    validation:validation,
    getFormData:getFormData,
};