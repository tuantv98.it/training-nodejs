const jwt = require("jsonwebtoken");
require("dotenv").config();
const ResponseJson = require('../support/ResponseResource.js');

const config = process.env;

const verifyToken = (req, res, next) => {
    const token =
        req.body.token || req.query.token || req.headers["authorization"];
    if (!token) {
        return ResponseJson.forbidden(res)
    }
    try {
        req.user = jwt.verify(token,config.SYSTEM_SECRET_KEY);
    } catch (err) {
        return ResponseJson.unauthorized(res)
    }
    return next();
};

module.exports = verifyToken;