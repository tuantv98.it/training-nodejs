const helper = require('../helper');
const ResponseJson = require("../support/ResponseResource");
const store = async (req, res, next) => {
    const validationRule = {
        "product_name": "required|string",
        "category_id": "required|integer",
        "price": "required|integer",
        "amount": "required|integer",
        "image": "string"
    };

    await helper.validation(req.body, validationRule, {}, (err, status) => {
        if (!status) {
            return ResponseJson.validation(res, err)
        }
        next();
    }).catch(err => console.log(err))
}
module.exports = {
    store: store
};