/**
 * Show result response
 * @param res
 * @param data
 */
const response = {
    response(res, data) {
        if (data) {
            res.status(200).json({
                code: 200,
                message: "Success",
                data: data
            })
        } else {
            res.status(500).json({
                code: 500,
                message: "An error occurred",
            })
        }
    },
    forbidden(res, error) {
        res.status(403).json({
            code: 403,
            message: "Forbidden",
            error: error,
        })
    },
    unauthorized(res, error) {
        res.status(401).json({
            code: 401,
            message: "Unauthorized",
            error: error,
        })
    },

    validation(res,error) {
        res.status(422).json({
            code: 422,
            message: "Validate error",
            ...error,
        })
    }
}

module.exports = response;
