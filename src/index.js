const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const dotenv = require('dotenv');
const initWebRoutes = require('./router/api.js');
const multer = require('multer');
const app = express();
const upload = multer({ dest: "uploads" });

const connectDB = require('./config/databases');

dotenv.config();

app.get('/',(req, res) => {
    res.send('Hello every body')
})

// for parsing application/json
app.use(bodyParser.json());

// for parsing application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: true}));
app.use(cors());

// for parsing form-data
app.use(upload.any())
app.use(express.static('public'))

//Route
initWebRoutes(app);

connectDB().then(r => {});

app.listen(3000,() => {
    console.log('Running serve port 3000')
})