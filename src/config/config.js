const dotenv = require('dotenv')
dotenv.config();

module.exports = {
    development: {
        username: process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_DATABASE,
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        dialect: 'mysql',
        dialectOptions: {
            supportBigNumbers: true,
            bigNumberStrings: true
        },
        logging: false
    },
    production: {
        username: process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_DATABASE,
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        dialect: 'mysql',
        dialectOptions: {
            supportBigNumbers: true,
            bigNumberStrings: true,
            // ssl: {
            //   ca: fs.readFileSync(__dirname + '/mysql-ca-main.crt')
            // }
        },
        logging: false
    }
};
