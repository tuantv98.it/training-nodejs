const express = require('express');
const Router = require('express-group-router');

const ProductController = require('../app/controller/ProductController.js');
const CategoryController = require('../app/controller/CategoryController.js');
const UserController = require('../app/controller/UserController.js');
const ProductValidate = require('../../src/app/validation/ProductValidate');
const verifyToken = require("../../src/app/middleware/verifyToken");

let router = new Router();
let login = new Router();
let initWebRoutes = (app) => {
    login.post('login', UserController.login)
    // group router product
    router.group('product/', (router) => {
        router.get('',ProductController.getProduct);
        router.post('store', ProductValidate.store, ProductController.store);
        router.delete(':product_id', ProductController.destroy);
        router.post('update', ProductController.update);
        router.get(':product_id', ProductController.getDetail);
    });

    // group router categories
    router.group('category/', (router) => {
        router.get('', CategoryController.getAll);
        router.post('store', CategoryController.store);
        router.delete(':product_id', CategoryController.destroy);
        router.post('update', CategoryController.update);
        router.get(':product_id', CategoryController.getDetail);
    });

    let listLogin = login.init();
    let listRoutes = router.init();
    app.use('/api/v1', listLogin)
    app.use('/api/v1', verifyToken, listRoutes)
}

module.exports = initWebRoutes;