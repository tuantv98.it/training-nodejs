'use strict';
const {Model} = require('sequelize');
const db = require('./index.js')
module.exports = (sequelize, DataTypes) => {
  class Categories extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({models}) {
    }
  }
  Categories.init({
    categories_name: DataTypes.STRING,
    avatar: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'categories',
  });

  return Categories;
};