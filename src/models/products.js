'use strict';
const {Model, Sequelize} = require('sequelize');
const db = require("./index.js");
module.exports = (sequelize, DataTypes) => {
  class Products extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({models}) {
    }
  }
  Products.init({
    product_name: DataTypes.STRING,
    image: DataTypes.STRING,
    amount: DataTypes.INTEGER,
    price: DataTypes.FLOAT,
    category_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'products',
    tableName: 'products',
    timestamps: true,
  });
  return Products;
};