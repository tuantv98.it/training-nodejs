FROM node:16

WORKDIR /

COPY package.json .

RUN apt-get update && apt-get install -y \
    build-essential \
    libpng-dev \
    libjpeg62-turbo-dev \
    libfreetype6-dev \
    locales \
    zip \
    jpegoptim optipng pngquant gifsicle \
    vim \
    unzip \
    git \
    curl \
    nano

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*


COPY . .
#
## Copy existing application directory permissions
#COPY --chown=www:www . /var/www/

RUN npm install

CMD npm run start
